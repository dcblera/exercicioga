#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


def generate_random_individual_with(features_length):
    """
    Generate a random binary-string individual.

    Argument:
    features_length -- length of the individual's features.
    """
    individual = [None] * features_length

    for i in range(features_length):
        individual[i] = str(random.choice([0, 1]))

    return ''.join(individual)


def generate_population(population_size, features_length):
    """
    Generate a population of random binary-string individuals.

    Arguments:
    population_size -- number of individuals.
    features_length -- the individual's length of bits/features.
    """
    population = []

    for i in range(population_size):
        population.append(generate_random_individual_with(features_length))

    return population


def get_fitness_from(individual):
    """
    Calculate the individual's fitness as the sum of its 1's bits.

    Argument:
    individual -- binary-string encoded individual.
    """
    fitness = 0

    for i in individual:
        fitness += int(i)

    return fitness


def get_fittest_from(population, fitness_function):
    """
    Returns the individual with the highest fitness.

    Arguments:
    population -- list of individuals.
    fitness_function -- callback function that calculates an individual's
            fitness value.
    """
    fittest_individual = population[0]
    highest_fitness = fitness_function(fittest_individual)

    for individual in population:
        current_fitness = fitness_function(individual)

        if current_fitness > highest_fitness:
            highest_fitness = current_fitness
            fittest_individual = individual

    return fittest_individual


def get_mating_pool_from(population, fitness_function, sample_size=10):
    """
    Returns a mating pool from a tournament-based selection.

    Arguments:
    population -- a list of at least ten string individuals regardless of
            the encoding.

    fitness_function -- callback function that calculates an individual's
            fitness value.

    sample_size -- the number of individuals choosen for each tournament,
            (default 10).
    """
    current_number = 1
    pop_size = len(population)
    mating_pool = []

    while(current_number <= pop_size):
        mating_pool.append(get_fittest_from(
            random.sample(population, sample_size), get_fitness_from))

        current_number += 1

    return mating_pool


def get_mutation_of(individual, rate):
    """
    Returns a new mutation from the given individual.

    Arguments:
    individual -- new binary-string individual.
    rate -- the mutation rate for each individual binary feature.
    """
    features = list(individual)

    for i in range(len(features)):
        if random.uniform(0.0, 1.0) < rate:
            features[i] = '1' if features[i] == '0' else '0'

    return ''.join(features)


def get_crossover_pair_from(parent1, parent2, rate):
    """
    Returns two offsprings from a one-point crossover.

    Arguments:
    parent1 -- a binary-string individual.
    parent2 -- same as above.
    rate -- crossover rate (usually 0.5 <= pc <= 1.0).
    """
    if random.uniform(0.0, 1.0) >= rate:
        return parent1, parent2

    xover_point = random.randint(1, len(parent1) - 1)
    offspring1 = parent1[0:xover_point] + parent2[xover_point:]
    offspring2 = parent2[0:xover_point] + parent1[xover_point:]

    return offspring1, offspring2


def optimum_solution_in(population):
    max_fitness = len(population[0])

    for i in population:
        if get_fitness_from(i) == max_fitness:
            return True

    return False


# DEBUGGING STUFF
def get_average_fitness(population, fitness_function):
    """
    Returns the population's average fitness.

    Arguments:
    population -- a list of at least ten string individuals regardless of
            the encoding.

    fitness_function -- callback function that calculates an individual's
            fitness value.
    """
    total_fitness = 0

    for i in population:
        total_fitness += fitness_function(i)

    return float(total_fitness) / len(population)


def get_optimum_individuals_count(population, fitness_function):
    """
    Returns the count of optimum-solution individuals where optimum means
    the sum of an individual's 1's bits.

    Arguments:
    population -- a list of at least ten string individuals regardless of
            the encoding.

    fitness_function -- callback function that calculates an individual's
            fitness value.
    """
    max_fitness = len(population[0])
    count = 0

    for i in population:
        if fitness_function(i) == max_fitness:
            count += 1

    return count


if __name__ == "__main__":
    MAX_GENERATIONS = 100
    POPULATION_SIZE = 100
    FEATURES_LENGTH = 25
    RECOMBINATION_RATE = 0.7
    MUTATION_RATE = 1.0 / FEATURES_LENGTH
    OPTIMUM_FITNESS = FEATURES_LENGTH

    print("--MISSION START!--")
    population = generate_population(POPULATION_SIZE, FEATURES_LENGTH)

    print("STARTING POPULATION:")

    print("Average fitness: {}".format(
        get_average_fitness(population, get_fitness_from)))

    for gen in range(MAX_GENERATIONS):
        mating_pool = get_mating_pool_from(population, get_fitness_from)
        new_population = []

        # Generational model
        for i in range(POPULATION_SIZE):
            parents = random.sample(mating_pool, 2)

            pair = get_crossover_pair_from(
                    parents[0], parents[1], RECOMBINATION_RATE)

            new_population.append(get_mutation_of(pair[0], MUTATION_RATE))
            new_population.append(get_mutation_of(pair[1], MUTATION_RATE))

        del population
        population = new_population
        del new_population
        del mating_pool

        if optimum_solution_in(population):
            print("\nOptimum solution found!")
            print("Number of generations: {}".format(gen + 1))

            print("Average fitness: {}".format(
                    get_average_fitness(population, get_fitness_from)))

            optimum_solutions = get_optimum_individuals_count(
                    population, get_fitness_from)

            print("Optimum individuals: {}".format(optimum_solutions))
            break

        print("\nGeneration: {}".format(gen + 1))
        print("Average fitness: {}".format(
            get_average_fitness(population, get_fitness_from)))

    print("--MISSION COMPLETE!--")
