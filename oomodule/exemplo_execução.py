#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Just a small test to show that the individuals' fitness tends to
# increase
from algorithm import *

pop_size = 10
mut_rate = 0.3
x_rate = 0.7

MAX_ITER = 1000

handler = IndividualHandler(pop_size, bit_sum, mut_rate, x_rate)

if __name__ == "__main__":
    genes = list("0000000000")
    length = len(genes)
    genes_list = []

    for i in range(length):
        genes_list.append(genes)

    population = Population(pop_size, handler)
    population.setup_individuals(genes_list, bit_sum)

    for gen in range(MAX_ITER):
        if population.get_optimal_individual():
            print("Solução ótima em {} gerações".format(i))
            break

        population.upgrade()

    for i in population.individuals:
        print(i)
    
