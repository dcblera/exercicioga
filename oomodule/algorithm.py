#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


def bit_sum(genes):
    fitness = 0

    for g in genes:
        fitness += int(g)

    return fitness


class Individual:
    __fmt_str__ = "[{}]"

    def __init__(self, genes, fitness_function):
        self.genes = genes
        self.fitness = fitness_function(self.genes)

    def __repr__(self):
        return self.__fmt_str__.format(','.join(self.genes))

    def __str__(self):
        return self.__repr__()


class IndividualHandler:
    def __init__(self,
            genotype_length, fitness_function, mutation_rate, crossover_rate):

        self.genotype_length = genotype_length
        self.fitness_function = fitness_function
        self.mutation_rate = mutation_rate
        self.xover_rate = crossover_rate

    def create_random_instance(self):
        genes = [None] * self.genotype_length

        for i in range(self.genotype_length):
            genes[i] = str(random.choice([0, 1]))

        return Individual(genes, self.fitness_function)

    def mutate(self, individual):
        genes = individual.genes[:]

        for i in range(self.genotype_length):
            if random.uniform(0.0, 1.0) < self.mutation_rate:
                genes[i] = '1' if genes[i] == '0' else '0'

        return Individual(genes, self.fitness_function)

    def get_crossover_pair_from(self, parent1, parent2):
        if random.uniform(0.0, 1.0) >= self.xover_rate:
            return parent1, parent2

        x_point = random.randint(1, self.genotype_length - 1)
        genes1 = parent1.genes[0:x_point] + parent2.genes[x_point:]
        genes2 = parent2.genes[0:x_point] + parent1.genes[x_point:]

        return [Individual(genes1, self.fitness_function),
                Individual(genes2, self.fitness_function)]


class Population:
    assert_length_msg = "population and 'genes_list' sizes differ ({0} vs {1})"

    def __init__(self, size, individual_handler):
        self.handler = individual_handler
        self.size = size
        self.individuals = [None] * self.size

    def random_init(self):
        for i in range(self.size):
            self.individuals[i] = self.handler.create_random_instance()

    def setup_individuals(self, genes_list, fitness_function):
        assert len(genes_list) == self.size, self.assert_length_msg.format(
                len(genes_list), self.size)

        for i in range(self.size):
            self.individuals[i] = Individual(genes_list[i], fitness_function)

    def setup_individualsALT(self, individuals):
        assert len(individuals) == self.size, self.assert_length_msg.format(
                len(individuals), self.size)

        self.individuals = individuals[:]

    def get_fittest_individual(self):
        return self.get_fittest_from(self.individuals)

    def get_fittest_from(self, individuals_list):
        fittest = individuals_list[0]
        highest = fittest.fitness

        for i in self.individuals:
            if i.fitness > highest:
                fittest = i
                highest = i.fitness

        return fittest


    def get_mating_pool(self, sample_size=10):
        current_number = 1
        pool = []

        while current_number <= self.size:
            pool.append(self.get_fittest_from(
                random.sample(self.individuals, sample_size)))

            current_number += 1

        return pool

    def get_optimal_individual(self):
        optimal_fitness = self.handler.genotype_length

        for i in self.individuals:
            if i.fitness == optimal_fitness:
                return i

        return []

    def upgrade(self):
        mating_pool = self.get_mating_pool(3)
        new_individuals = []
        handler = self.handler

        for i in range(int(self.size / 2)):
            parents = random.sample(mating_pool, 2)
            pair = handler.get_crossover_pair_from(parents[0], parents[1])

            new_individuals.append(handler.mutate(pair[0]))
            new_individuals.append(handler.mutate(pair[1]))

        self.setup_individualsALT(new_individuals)

