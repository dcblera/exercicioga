#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from unittest import TestCase

from algorithm import *


class IndividualTesting(TestCase):
    def test_fitness_calculation(self):
        genes = list(bin(random.randint(0, 2000000))[2:])
        
        fitness = 0
        for g in genes:
            fitness += int(g)

        self.assertEqual(fitness, bit_sum(genes))

        genes = list('1111')
        self.assertEqual(4, bit_sum(genes))

    def test_initialization(self):
        genes = list("00000")
        individual = Individual(genes, bit_sum)

        self.assertTrue(isinstance(individual, Individual))
        self.assertEqual(genes, individual.genes)

    def test_representation(self):
        length = random.randint(0, 30)
        genes = [None] * length

        for i in range(length):
            genes[i] = random.choice(['0', '1'])

        individual = Individual(genes, bit_sum)

        self.assertTrue(isinstance(individual.__repr__(), str))
        self.assertTrue(isinstance(individual.__str__(), str))

        representation = "[{}]".format(','.join(genes))

        self.assertEqual(representation, individual.__repr__())
        self.assertEqual(representation, individual.__str__())


class IndividualHandlerTesting(TestCase):
    def test_handler_init(self):
        genotype_length = 10
        mutation_rate = 0.0
        crossover_rate = 0.0

        handler = IndividualHandler(
                genotype_length, bit_sum, mutation_rate, crossover_rate)

        self.assertTrue(isinstance(handler, IndividualHandler))

    def test_random_instance_creation(self):
        genotype_length = 10
        mutation_rate = 0.0
        crossover_rate = 0.0

        handler = IndividualHandler(
                genotype_length, bit_sum, mutation_rate, crossover_rate)

        individual = handler.create_random_instance()

        self.assertTrue(isinstance(individual, Individual))

    def test_mutation(self):
        genotype_length = 10
        mutation_rate = 0.0
        crossover_rate = 0.0

        handler = IndividualHandler(
                genotype_length, bit_sum, mutation_rate, crossover_rate)

        individual = handler.create_random_instance()
        mutation = handler.mutate(individual)

        self.assertTrue(isinstance(mutation, Individual))

    def test_crossover(self):
        genotype_length = 10
        mutation_rate = 0.0
        crossover_rate = 0.0

        handler = IndividualHandler(
                genotype_length, bit_sum, mutation_rate, crossover_rate)
        
        parent1 = Individual(list("00000"), bit_sum)
        parent2 = Individual(list("11111"), bit_sum)
        pair = handler.get_crossover_pair_from(parent1, parent2)

        self.assertEqual(len(pair), 2)
        self.assertEqual(parent1.genes, pair[0].genes)
        self.assertEqual(parent2.genes, pair[1].genes)
        
        for i in pair:
            self.assertTrue(isinstance(i, Individual))

class PopulationTesting(TestCase):
    def setUp(self):
        self.length = 10
        self.mut_rate = 0.0
        self.xover_rate = 0.0
        
        self.handler = IndividualHandler(
                self.length, bit_sum, self.mut_rate, self.xover_rate)

    def tearDown(self):
        del self.length, self.mut_rate, self.xover_rate, self.handler

    def test_init_params(self):
        size = 10
        population = Population(size, self.handler)

        self.assertTrue(isinstance(population, Population))
        self.assertEqual(size, population.size)

    def test_random_initialization(self):
        size = 10
        population = Population(size, self.handler)
        population.random_init()

        self.assertEqual(size, population.size)

        for i in population.individuals:
            self.assertTrue(isinstance(i, Individual))

    def test_individuals_setup(self):
        size = 10
        population = Population(size, self.handler)

        gene = list("0000000000")
        length = len(gene)
        genes_list = []

        for i in range(length - 1):
            genes_list.append(gene)

        self.assertRaises(
            AssertionError, population.setup_individuals, genes_list, bit_sum)

        genes_list.append('1111111111')
        population.setup_individuals(genes_list, bit_sum)

        for i in population.individuals:
            self.assertTrue(isinstance(i, Individual))

    def test_fittest_finder(self):
        size = 5
        population = Population(size, self.handler)

        gene = list("00000")
        length = len(gene)
        genes_list = []

        for i in range(length - 1):
            genes_list.append(gene)

        fittest_gene = list("11111")
        genes_list.append(fittest_gene)
        population.setup_individuals(genes_list, bit_sum)

        fittest = population.get_fittest_individual()
        self.assertTrue(isinstance(fittest, Individual))
        self.assertEqual(fittest.genes, fittest_gene)

    def test_mating_pool_creation(self):
        gene = list("00000")
        length = len(gene)
        genes_list = []

        for i in range(length):
            genes_list.append(gene)

        size = 5
        population = Population(size, self.handler)
        population.setup_individuals(genes_list, bit_sum)
        pool = population.get_mating_pool(3)

        for i in pool:
            self.assertTrue(isinstance(i, Individual))

    def test_selection_upgrade(self):
        genes = list("0000")
        length = len(genes)
        genes_list = []

        for i in range(length):
            genes_list.append(genes)

        size = length
        population = Population(size, self.handler)
        population.setup_individuals(genes_list, bit_sum)

        population.upgrade()

        self.assertEqual(size, len(population.individuals))

    def test_get_optimal_individual_fail(self):
        genes = list("0000000000")
        length = len(genes)
        genes_list = []

        for i in range(length):
            genes_list.append(genes)

        size = length
        population = Population(size, self.handler)
        population.setup_individuals(genes_list, bit_sum)

        self.assertTrue(population.get_optimal_individual() == [])

    def test_get_optimal_individual_success(self):
        genes = list("1111111111")
        length = len(genes)
        genes_list = []

        for i in range(length):
            genes_list.append(genes)

        size = length
        population = Population(size, self.handler)
        population.setup_individuals(genes_list, bit_sum)

        self.assertTrue(population.get_optimal_individual() != [])

