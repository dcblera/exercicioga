#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from unittest import TestCase

from genetic_algo import *


class PopulationTests(TestCase):
    def test_generation(self):
        population_size = random.randint(10, 100)
        features_length = random.randint(1, 50)

        population = generate_population(population_size, features_length)

        self.assertEqual(len(population), population_size)
        self.assertEqual(len(population[0]), features_length)

    def test_mating_pool_creation(self):
        population_size = random.randint(10, 100)
        features_length = random.randint(1, 50)

        population = generate_population(population_size, features_length)
        mating_pool = get_mating_pool_from(population, get_fitness_from)

        self.assertEqual(len(population), len(mating_pool))

    def test_crossover(self):
        population_size = random.randint(10, 100)
        features_length = random.randint(1, 50)

        population = generate_population(population_size, features_length)
        mating_pool = get_mating_pool_from(population, get_fitness_from)
        parents = random.sample(population, 2)

        xover_rate = random.uniform(0.0, 1.0)
        xover_pair = get_crossover_pair_from(parents[0], parents[1], xover_rate)

        self.assertEqual(len(xover_pair), 2)
        self.assertEqual(len(xover_pair[0]), len(xover_pair[1]))
        self.assertEqual(len(xover_pair[0]), len(parents[0]))

        # Test the case where the crossover doesn't happen
        xover_rate = -1.0
        xover_pair = get_crossover_pair_from(parents[0], parents[1], xover_rate)

        self.assertEqual(len(xover_pair), 2)
        self.assertEqual(len(xover_pair[0]), len(xover_pair[1]))
        self.assertEqual(len(xover_pair[0]), len(parents[0]))
        self.assertEqual(xover_pair[0], parents[0])
        self.assertEqual(xover_pair[1], parents[1])

    def test_optimum_solution_find(self):
        population = []
        population.append("0000")

        self.assertEqual(optimum_solution_in(population), False)
        population.append("1111")
        self.assertEqual(optimum_solution_in(population), True)

    def test_average_fitness_calculation(self):
        population = []
        population.append("0000")
        population.append("0001")
        population.append("0011")
        population.append("0111")
        population.append("1111")

        self.assertEqual(get_average_fitness(population, get_fitness_from),
                         10.0 / len(population))

    def test_optimum_individuals_list(self):
        population = []
        population.append("0000")
        population.append("0001")
        population.append("0011")
        population.append("0111")
        population.append("1111")
        population.append("1111")

        optimum_count = get_optimum_individuals_count(
                population, get_fitness_from)

        self.assertEqual(optimum_count, 2)


class IndividualTests(TestCase):
    def test_random_creation(self):
        length = random.randint(1, 32)
        individual = generate_random_individual_with(length)

        self.assertEqual(type(individual), type(""))
        self.assertEqual(len(individual), length)

    def test_fitness_calc(self):
        individual = bin(random.randint(0, 2000000))[2:]

        fitness = 0
        for i in individual:
            fitness += int(i)

        self.assertEqual(get_fitness_from(individual), fitness)
        del individual

    def test_mutation(self):
        individual = bin(random.randint(0, 2000000))[2:]
        mutation = get_mutation_of(individual, 1.0 / len(individual))

        self.assertEqual(len(mutation), len(individual))
